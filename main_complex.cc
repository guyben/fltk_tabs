//
// main.cc
//
// Usage example for Fl_Drag_Tabs for the Fast Light Tool Kit (FLTK).
//
// Copyright by Guy Bensky
//
// This library is free software. Distribution and use rights are outlined in
// the file "COPYING" which should have been included with this file.  If this
// file is missing or damaged, see the license at:
//
//     http://www.fltk.org/COPYING.php
//
// Please report all bugs and problems on the following page:
//
//     http://www.fltk.org/str.php
//

#include "Fl_Drag_Tabs.H"
#include <FL/Fl_Window.H>
#include <FL/Fl_Group.H>
#include <FL/Fl_Button.H>
#include <FL/Fl.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Input.H>
#include <FL/Fl_Group.H>
#include <FL/fl_draw.H>

class Fl_Browser:public Fl_Group{
  public:
    Fl_Browser(int xx, int yy, int ww, int hh):Fl_Group(xx,yy,ww,hh){
      Fl_Group *header=new Fl_Group(0,10,ww,30);
      in=new Fl_Input(50,10,ww-50-30-30,30,"Color");
      in->callback(in_cb,this);
      btn_p=new Fl_Button(ww-30-30,10,30,30,"+");
      btn_p->callback(btn_p_cb,this);
      btn_m=new Fl_Button(ww-30,10,30,30,"-");
      btn_m->callback(btn_m_cb,this);
      header->end();
      header->resizable(in);
      tabs=new Fl_Drag_Tabs(0,50,ww,hh-50);
      tabs->creator(&Fl_Browser::tab_creator,NULL);
      //tabs->selection_color(FL_RED);
      tabs->end();
      tabs->callback(tabs_cb,this);
      fix_values();
    }
    int handle(int event){
      int state=Fl::event_state();
      state &= FL_CTRL | FL_ALT | FL_SHIFT;
      switch(event){
        case FL_KEYDOWN:
          switch (Fl::event_key()){
            case 't':
              if (state == FL_CTRL){
                this->add_tab();
                return 1;
              }
              break;
            case FL_Tab:
              if (state == FL_CTRL){
                if (tabs->children()){
                  int i=tabs->find(tabs->value());
                  ++i;
                  if (i==tabs->children())
                    i=0;
                  tabs->value(tabs->child(i));
                  tabs->do_callback();
                }
                return 1;
              }else if (state == (FL_CTRL | FL_SHIFT)){
                if (tabs->children()){
                  int i=tabs->find(tabs->value());
                  --i;
                  if (i==-1)
                    i=tabs->children()-1;
                  tabs->value(tabs->child(i));
                  tabs->do_callback();
                }
                return 1;
              }
              break;
            case 'l':
              if ((state == FL_CTRL) && (in->active())){
                Fl::focus(in);
                in->position(in->maximum_size(),0);
                return 1;
              }
              break;
            case 'w':
              if (state == FL_CTRL){
                if (tabs->children())
                  remove_tab();
                return 1;
              }
              break;
            default:
              break;
          }
          break;
        default:
          break;
      };
      return Fl_Group::handle(event);
    }
    static Fl_Drag_Tabs *tab_creator(int x, int y, void *){
      Fl_Window *wnd=new Fl_Window(x,y,640,480,"Browser Window");
      Fl_Browser *brs=new Fl_Browser(0,0,640,480);
      wnd->end();
      wnd->show();
      wnd->resizable(brs);
      wnd->size_range(100,100);
      return brs->tabs;
    }


  private:
    static void in_cb(Fl_Widget *wgt, void *v){
      Fl_Browser *b=(Fl_Browser*)v;
      b->in_cb_i();
    }
    void fix_values(){
      if (tabs->children()==0){
        in->value("Add tabs using button, then set color in hex");
        window()->label("Fl_Drag_Tabs example");
        in->deactivate();
        btn_m->deactivate();
        return;
      }
      in->activate();
      btn_m->activate();
      in->value(tabs->value()->label());
      window()->copy_label(tabs->value()->label());
      window()->damage(FL_DAMAGE_ALL,x(),y(),w(),h());
    }
    void in_cb_i(){
      set_address(in->value());
    }
    void set_address(const char *txt){
      char *e;
      long c=strtol(txt,&e,16);
      if ((*e)||(c>0xFFFFFF)){
        fix_values();
        return;
      }
      tabs->value()->copy_label(txt);
      tabs->value()->color(fl_rgb_color((c>>16)&0xFF,(c>>8)&0xFF,c&0xFF));
      fix_values();
    }
    static void btn_p_cb(Fl_Widget *wgt, void *v){
      Fl_Browser *b=(Fl_Browser*)v;
      b->add_tab();
    }
    void add_tab(){
      int xx,yy,ww,hh;
      tabs->client_area(xx,yy,ww,hh,0);
      Fl_Box *box=new Fl_Box(xx,yy,ww,hh,"000000");
      box->box(FL_FLAT_BOX);
      tabs->add(box);
      tabs->value(box);
      char t[7];
      for (int i=0;i<6;++i)
        t[i]="0123456789abcdef"[random()%16];
      t[6]=0;
      set_address(t);
    }
    static void btn_m_cb(Fl_Widget *wgt, void *v){
      Fl_Browser *b=(Fl_Browser*)v;
      b->remove_tab();
    }
    void remove_tab(){
      Fl_Widget *wgt=tabs->value();
      int i=tabs->find(wgt);
      tabs->remove(wgt);
      delete wgt;
      if (i==tabs->children())
        --i;
      if (i<0)
        add_tab();
      else{
        tabs->value(tabs->child(i));
        fix_values();
      }
    }
    static void tabs_cb(Fl_Widget *wgt, void *v){
      Fl_Browser *b=(Fl_Browser*)v;
      b->fix_values();
    }
    Fl_Input *in;
    Fl_Drag_Tabs *tabs;
    Fl_Button *btn_p,*btn_m;
};

int main(){
  Fl_Drag_Tabs *tbs=Fl_Browser::tab_creator(200,200,NULL);
  tbs->deletable(true);
  Fl::run();
  return 0;
}
