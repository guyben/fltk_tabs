//
// main.cc
//
// Usage example for Fl_Drag_Tabs for the Fast Light Tool Kit (FLTK).
//
// Copyright by Guy Bensky
//
// This library is free software. Distribution and use rights are outlined in
// the file "COPYING" which should have been included with this file.  If this
// file is missing or damaged, see the license at:
//
//     http://www.fltk.org/COPYING.php
//
// Please report all bugs and problems on the following page:
//
//     http://www.fltk.org/str.php
//

#include "Fl_Drag_Tabs.H"
#include <FL/Fl_Window.H>
#include <FL/Fl.H>
#include <FL/Fl_Box.H>
#include <FL/Enumerations.H>

int main(){
  {
    Fl_Window *window=new Fl_Window(640,480,"Tabbing test2");
    Fl_Drag_Tabs *tabs=new Fl_Drag_Tabs(0,0,640,480,"red green blue");
    Fl_Box *box;
    box=new Fl_Box(0,30,640,480-30,"red")  ;box->color(FL_RED)  ;box->box(FL_NO_BOX);
    box=new Fl_Box(0,30,640,480-30,"green");box->color(FL_GREEN);box->box(FL_NO_BOX);
    box=new Fl_Box(0,30,640,480-30,"blue") ;box->color(FL_BLUE) ;box->box(FL_NO_BOX);
    tabs->end();
    tabs->deletable(true);
    //tabs->selection_color(FL_RED);
    window->end();
    window->resizable(tabs);
    window->show();
  }
  Fl::run();
  return 0;
}
