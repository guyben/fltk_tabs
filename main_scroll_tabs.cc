#include "Fl_Scroll_Tabs.H"
#include <FL/Fl.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Window.H>

int main(){
  Fl_Window *wnd=new Fl_Window(640,480,"Fl_Scroll_Tabs example");
  Fl_Scroll_Tabs *tabs=new Fl_Scroll_Tabs(0,0,640,480);
  int X,Y,W,H;
  tabs->client_area(X,Y,W,H);
  Fl_Box *box;
  box=new Fl_Box(X,Y,W,H,"BLACK");box->color(FL_BLACK);
  box=new Fl_Box(X,Y,W,H,"BLACK");box->color(FL_BLACK);
  box=new Fl_Box(X,Y,W,H,"RED");box->color(FL_RED);
  box=new Fl_Box(X,Y,W,H,"GREEN");box->color(FL_GREEN);
  box=new Fl_Box(X,Y,W,H,"YELLOW");box->color(FL_YELLOW);
  box=new Fl_Box(X,Y,W,H,"BLUE");box->color(FL_BLUE);
  box=new Fl_Box(X,Y,W,H,"MAGENTA");box->color(FL_MAGENTA);
  box=new Fl_Box(X,Y,W,H,"CYAN");box->color(FL_CYAN);
  //box=new Fl_Box(X,Y,W,H,"DARK RED");box->color(FL_DARK_RED);
  //box=new Fl_Box(X,Y,W,H,"DARK GREEN");box->color(FL_DARK_GREEN);
  //box=new Fl_Box(X,Y,W,H,"DARK YELLOW");box->color(FL_DARK_YELLOW);
  //box=new Fl_Box(X,Y,W,H,"DARK BLUE");box->color(FL_DARK_BLUE);
  //box=new Fl_Box(X,Y,W,H,"DARK MAGENTA");box->color(FL_DARK_MAGENTA);
  //box=new Fl_Box(X,Y,W,H,"DARK CYAN");box->color(FL_DARK_CYAN);
  box=new Fl_Box(X,Y,W,H,"WHITE");box->color(FL_WHITE);
  tabs->end();
  tabs->minw(75);
  tabs->maxw(100);
  wnd->end();
  wnd->resizable(tabs);
  //wnd->size(100,100);
  wnd->show();
  Fl::run();
  return 0;
}
